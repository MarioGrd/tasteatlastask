﻿using Rest.DAL.Infrastructure.GenericRepository;
using Rest.DAL.Infrastructure.GenericRepository.Interfaces;
using System;
using System.Threading.Tasks;

namespace Rest.DAL.Infrastructure.Common
{
    public class UnitOfWork : IUnitOfWork
    {
        private RestaurantContext _dbContext;
        protected IDbFactory _dbFactory;

        protected RestaurantContext DbContext
        {
            get
            {
                return this._dbContext ?? (this._dbContext = this._dbFactory.Initialize());
            }
        }

        public IRestaurantRepository RestaurantRepository { get; private set; }
        public ICityRepository CityRepository { get; private set; }

        public UnitOfWork(IDbFactory dbFactory)
        {
            this._dbFactory = dbFactory;

            this.RestaurantRepository = new RestaurantRepository(this.DbContext);
            this.CityRepository = new CityRepository(this.DbContext);
        }

        public async Task CommitAsync()
        {
            await this._dbContext.SaveChangesAsync();
        }
    }
}
