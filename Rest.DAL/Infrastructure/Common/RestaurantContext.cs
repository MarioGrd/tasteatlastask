﻿using Rest.DAL.Infrastructure.Configuration;
using Rest.Shared.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Threading.Tasks;

namespace Rest.DAL.Infrastructure.Common
{
    public class RestaurantContext : DbContext
    {
        public RestaurantContext() : base("RestApp")
        {
            Database.SetInitializer<RestaurantContext>(null);
        }

        public IDbSet<Restaurant> Restaurants { get; set; }
        public IDbSet<City> Cities { get; set; }

        public virtual async Task CommitAsync()
        {
            await base.SaveChangesAsync();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Configurations.Add(new RestaurantConfiguration());
            modelBuilder.Configurations.Add(new CityConfiguration());
        }
    }
}
