﻿using System;

namespace Rest.DAL.Infrastructure.Common
{
    public interface IDbFactory : IDisposable
    {
        RestaurantContext Initialize();
    }
}
