﻿namespace Rest.DAL.Infrastructure.Common
{
    public class DbFactory : Disposable, IDbFactory
    {
        private RestaurantContext _dbContext;

        public RestaurantContext Initialize()
        {
            return this._dbContext ?? (this._dbContext = new RestaurantContext());
        }

        protected override void DisposeCore()
        {
            if (this._dbContext != null)
                this._dbContext.Dispose();
        }
    }
}
