﻿using Rest.DAL.Infrastructure.GenericRepository.Interfaces;
using System;
using System.Threading.Tasks;

namespace Rest.DAL.Infrastructure.Common
{
    public interface IUnitOfWork
    {
        IRestaurantRepository RestaurantRepository { get; }
        ICityRepository CityRepository { get; }
        Task CommitAsync();
    }
}
