﻿using Rest.DAL.Infrastructure.Common;
using Rest.DAL.Infrastructure.GenericRepository.Interfaces;
using Rest.Shared.Models;

namespace Rest.DAL.Infrastructure.GenericRepository
{
    public class CityRepository : Repository<City>, ICityRepository
    {
        public CityRepository(RestaurantContext dbContext) : base(dbContext)
        {
        }
    }
}
