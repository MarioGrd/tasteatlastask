﻿using Rest.DAL.Infrastructure.Common;
using Rest.DAL.Infrastructure.GenericRepository.Interfaces;
using Rest.Shared.Common;
using Rest.Shared.Models;
using Rest.Shared.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Rest.DAL.Infrastructure.GenericRepository
{
    public class RestaurantRepository : Repository<Restaurant>, IRestaurantRepository
    {
        public RestaurantRepository(RestaurantContext dbContext) : base(dbContext)
        {
        }

        /// <summary>
        /// Filters restaurants by city.
        /// </summary>
        /// <param name="cityId">City id.</param>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Paged response of restaurants</returns>
        public async Task<PagedResponse<Restaurant>> FilterByCityAsync(Guid cityId, int pageIndex, int pageSize)
        {
            var skip = (pageIndex - 1) * pageSize;

            var restaurants =
                await this._dbSet
                    .Where(r => r.CityId == cityId)
                    .OrderBy(r => r.Name)
                    .Skip(skip)
                    .Take(pageSize)
                    .ToListAsync();

            var count = await this._dbSet.Where(r => r.CityId == cityId).CountAsync();

            return new PagedResponse<Restaurant>()
            {
                Data = restaurants,
                Count = count
            };
        }

        /// <summary>
        /// Checks if location exists.
        /// </summary>
        /// <param name="longitude">Longitude.</param>
        /// <param name="latitude">Latitude.</param>
        /// <returns></returns>
        public async Task<bool> LocationExists(float longitude, float latitude)
        {
            return await this._dbSet.AnyAsync(r => r.Longitude == longitude && r.Latitude == latitude);
        }


        /// <summary>
        /// Gets nearest restaurants.
        /// </summary>
        /// <param name="restaurantId">Restaurant id.</param>
        /// <returns></returns>
        public async Task<List<RestaurantView>> GetNearestRestourants(Guid restaurantId)
        {
            int countNumber = 4;

            return
                await this._dbContext.Database.SqlQuery<RestaurantView>(
                    "[proc].FindNearest @Id, @RestaurantCount",
                    new SqlParameter("@Id", SqlDbType.UniqueIdentifier) { Value = restaurantId },
                    new SqlParameter("@RestaurantCount", SqlDbType.Int) { Value = countNumber }).ToListAsync();
        }
    }
}
