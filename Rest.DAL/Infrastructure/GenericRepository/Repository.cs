﻿using Rest.DAL.Infrastructure.GenericRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Rest.DAL.Infrastructure.GenericRepository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity: class
    {
        protected DbContext _dbContext;
        protected readonly DbSet<TEntity> _dbSet;

        public Repository(DbContext dbContext)
        {
            this._dbContext = dbContext;
            this._dbSet = this._dbContext.Set<TEntity>();
        }

        /// <summary>
        /// Checks if any entity satisfies condition.
        /// </summary>
        /// <param name="predicate">Predicate</param>
        /// <returns></returns>
        public async Task<bool> AnyAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await this._dbSet.AnyAsync(predicate);
        }

        /// <summary>
        /// Gets single entity.
        /// </summary>
        /// <param name="id">Entity id.</param>
        /// <returns>Single entity.</returns>
        public async Task<TEntity> GetAsync(Guid id)
        {
            return await this._dbSet.FindAsync(id);
        }

        /// <summary>
        /// Gets all entities.
        /// </summary>
        /// <returns>List of entities.</returns>
        public async Task<List<TEntity>> GetAllAsync()
        {
            return await this._dbSet.ToListAsync();
        }

        /// <summary>
        /// Adds an entity to database.
        /// </summary>
        /// <param name="entity">Entity</param>
        public void Add(TEntity entity)
        {
            this._dbContext.Entry(entity).State = EntityState.Added;
            this._dbSet.Add(entity);
        }
    }
}
