﻿using Rest.Shared.Models;

namespace Rest.DAL.Infrastructure.GenericRepository.Interfaces
{
    public interface ICityRepository : IRepository<City>
    {
    }
}
