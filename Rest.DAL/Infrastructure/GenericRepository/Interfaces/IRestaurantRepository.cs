﻿using Rest.Shared.Common;
using Rest.Shared.Models;
using Rest.Shared.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rest.DAL.Infrastructure.GenericRepository.Interfaces
{
    public interface IRestaurantRepository : IRepository<Restaurant>
    {
        Task<PagedResponse<Restaurant>> FilterByCityAsync(Guid cityId, int pageIndex, int pageSize);
        Task<List<RestaurantView>> GetNearestRestourants(Guid restaurantId);
        Task<bool> LocationExists(float longitude, float latitude);
    }
}
