﻿using Rest.Shared.Models;
using System.Data.Entity.ModelConfiguration;

namespace Rest.DAL.Infrastructure.Configuration
{
    public class RestaurantConfiguration : EntityTypeConfiguration<Restaurant>
    {
        public RestaurantConfiguration()
        {
            this.HasKey(k => k.Id);

            this.Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(Restaurant.MaxNameLength);

            this.Property(p => p.Adress)
                .IsRequired()
                .HasMaxLength(Restaurant.MaxAdressLength);

            this.Property(p => p.Longitude)
                .IsRequired();

            this.Property(p => p.Latitude)
                .IsRequired();
        }
    }
}
