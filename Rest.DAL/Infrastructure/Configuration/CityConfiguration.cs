﻿using Rest.Shared.Models;
using System.Data.Entity.ModelConfiguration;

namespace Rest.DAL.Infrastructure.Configuration
{
    public class CityConfiguration : EntityTypeConfiguration<City>
    {
        public CityConfiguration()
        {
            this.HasKey(e => e.Id);
            this.Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(City.MaxNameLength);

            this.Property(p => p.Country)
                .IsRequired()
                .HasMaxLength(City.MaxCountryLength);
        }
    }
}
