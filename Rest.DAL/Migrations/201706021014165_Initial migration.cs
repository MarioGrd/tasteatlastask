namespace Rest.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initialmigration : DbMigration
    {
        public override void Up()
        {
            // Scheme for table.
            Sql(
                @"IF NOT EXISTS(SELECT * FROM sys.schemas WHERE name = 'tbl')
                    BEGIN
                    EXECUTE('CREATE SCHEMA [tbl]');
                    END
                    "
                );

            // Scheme for funtion.
            Sql(
                @"IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = 'fn')
                    BEGIN
                    EXECUTE('CREATE SCHEMA [fn]');
                    END
                    "
                );

            // Scheme for type.
            Sql(
                @"IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = 'type')
                    BEGIN
	                EXECUTE ('CREATE SCHEMA [type]');
                    END
                    "
                );

            // Scheme for stored procedure.
            Sql(
                @"IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = 'proc')
                    BEGIN
	                EXECUTE ('CREATE SCHEMA [proc]');
                    END
                    "
                );

            // Calculate distance function.
            Sql(
                @"
                  CREATE FUNCTION [fn].[CalculateDistanceKm]
                    (
                    	@Latitude1	FLOAT,
                    	@Longitude1 FLOAT,
                    	@Latitude2	FLOAT,
                    	@Longitude2 FLOAT
                    )
                    	RETURNS FLOAT
                  AS
                  BEGIN
                    
                    	RETURN
                    		ACOS(
                    			SIN(PI() * @Latitude1 / 180.0) *
                    			SIN(PI() * @Latitude2 / 180.0) +
                    			COS(PI() * @Latitude1 / 180.0) *
                    			COS(PI() * @Latitude2 / 180.0) *
                    			COS(PI() * @Longitude2 / 180.0 - PI() * @Longitude1 / 180.0)
                    		) * 6371;
                    
                  END"
                );

            // Find nearest stored procedure.
            Sql(
                @"IF NOT EXISTS(SELECT TOP 1 1 FROM sys.types WHERE name = '[type].[Restaurant]')
                    BEGIN
                    	CREATE TYPE [type].[Restaurant] AS TABLE
                    	(
                    		Id UNIQUEIDENTIFIER,
                    		Name NVARCHAR(50),
                    		Adress NVARCHAR(50),
                    		Longitude FLOAT,
                    		Latitude FLOAT,
                    		CityId UNIQUEIDENTIFIER
                    
                    	);
                    END"
                );

            Sql(
                @"
                    CREATE PROCEDURE [proc].[FindNearest]
                    	@Id				 UNIQUEIDENTIFIER,
                    	@RestaurantCount INTEGER
                    AS
                    BEGIN
                    
                    	DECLARE @CurrentRestaurant [type].[Restaurant];
                    
                    INSERT INTO @CurrentRestaurant
                    	SELECT
                    	    	 r.Id,
                    			 r.Name,
                    			 r.Adress,
                    			 r.Longitude,
                    			 r.Latitude,
                    			 r.CityId
                    	FROM	
                    			dbo.Restaurant r
                    	WHERE	
                    			r.Id = @Id;
                    
                    	SELECT TOP (@RestaurantCount)
                    		 r.Id,
                    		 r.Name,
                    		 r.Adress,
                    		 r.Longitude,
                    		 r.Latitude,
                    		 fn.CalculateDistanceKm(
                    			(SELECT Latitude FROM @CurrentRestaurant),
                    			(SELECT Longitude FROM @CurrentRestaurant),
                    			Latitude,
                    			Longitude) AS Distance,
                    		r.CityId,
                    
                    		c.Name as CityName,
                    		c.Country as Country
                    	FROM
                    			dbo.Restaurant r
                    				INNER JOIN dbo.City c ON c.Id = r.CityId
                    	WHERE
                    			CityId = (SELECT CityId FROM @CurrentRestaurant) AND r.Id <> @Id
                    	ORDER
                    	BY		Distance;
                    
                    END"
                );

            CreateTable(
                "dbo.City",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50),
                        Country = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Restaurant",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50),
                        Adress = c.String(nullable: false, maxLength: 50),
                        Longitude = c.Single(nullable: false),
                        Latitude = c.Single(nullable: false),
                        CityId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.City", t => t.CityId, cascadeDelete: true)
                .Index(t => t.CityId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Restaurant", "CityId", "dbo.City");
            DropIndex("dbo.Restaurant", new[] { "CityId" });
            DropTable("dbo.Restaurant");
            DropTable("dbo.City");
            DropStoredProcedure("[proc].[FindNearest]");
        }
    }
}
