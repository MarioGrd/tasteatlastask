﻿namespace Rest.DAL.Migrations
{
    using Rest.DAL.Infrastructure.Common;
    using Rest.Shared.Models;
    using System;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<RestaurantContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(RestaurantContext context)
        {
            var cities = this.GenerateCities();

            context.Cities.AddOrUpdate(cities);

            context.Restaurants.AddOrUpdate(this.GenerateRestaurants(cities));
        }

        private City[] GenerateCities()
        {
            return new City[]
            {
                new City() { Id = Guid.NewGuid(), Name = "Sisak", Country = "Croatia" },
                new City() { Id = Guid.NewGuid(), Name = "Zagreb", Country = "Croatia" },
                new City() { Id = Guid.NewGuid(), Name = "Osijek", Country = "Croatia" },
                new City() { Id = Guid.NewGuid(), Name = "Rijeka", Country = "Croatia" },
                new City() { Id = Guid.NewGuid(), Name = "Split", Country = "Croatia" },
            };
        }

        private Restaurant[] GenerateRestaurants(City[] cities)
        {
            return new Restaurant[]
            {
                new Restaurant()
                {
                    Id = Guid.NewGuid(),
                    Name = "Cocktail",
                    Adress = "Treća ulica",
                    Longitude = 16.374212f,
                    Latitude = 45.487858f,
                    CityId = cities[0].Id,
                    City = cities[0]
                },
                new Restaurant()
                {
                    Id = Guid.NewGuid(),
                    Name = "Pizzeria 044",
                    Adress = "Druga ulica",
                    Longitude = 16.372752f,
                    Latitude = 45.487201f,
                    CityId = cities[0].Id,
                    City = cities[0]
                },
                new Restaurant()
                {
                    Id = Guid.NewGuid(),
                    Name = "Mali kaptol",
                    Adress = "Prva ulica",
                    Longitude = 16.370705f,
                    Latitude = 45.487444f,
                    CityId = cities[0].Id,
                    City = cities[0]
                },
                new Restaurant()
                {
                    Id = Guid.NewGuid(),
                    Name = "Žurnal",
                    Adress = "Frankopansa ulica",
                    Longitude = 16.372385f,
                    Latitude = 45.491287f,
                    CityId = cities[0].Id,
                    City = cities[0]
                },
                new Restaurant()
                {
                    Id = Guid.NewGuid(),
                    Name = "Gušti",
                    Adress = "Tamo negdje kod placa",
                    Longitude = 16.379137f,
                    Latitude = 45.486293f,
                    CityId = cities[0].Id,
                    City = cities[0]
                },
                new Restaurant()
                {
                    Id = Guid.NewGuid(),
                    Name = "Grill",
                    Adress = "Negdje na viktorovcu",
                    Longitude = 16.367543f,
                    Latitude = 45.476591f,
                    CityId = cities[0].Id,
                    City = cities[0]
                },
                new Restaurant()
                {
                    Id = Guid.NewGuid(),
                    Name = "Picass Zagreb",
                    Adress = "Picass Zagreb street",
                    Longitude = 16.020424f,
                    Latitude = 45.806522f,
                    CityId = cities[1].Id,
                    City = cities[1]
                },
                 new Restaurant()
                {
                    Id = Guid.NewGuid(),
                    Name = "Pitzeria i spagetaria",
                    Adress = "Pitzeria i spagetaria uslica",
                    Longitude = 15.978377f,
                    Latitude = 45.814994f,
                    CityId = cities[1].Id,
                    City = cities[1]
                },
                new Restaurant()
                {
                    Id = Guid.NewGuid(),
                    Name = "Batak grill",
                    Adress = "Batak grill 49a",
                    Longitude = 15.9925f,
                    Latitude = 45.794419f,
                    CityId = cities[1].Id,
                    City = cities[1]
                },
                new Restaurant()
                {
                    Id = Guid.NewGuid(),
                    Name = "Restoran Mr.Chen",
                    Adress = "Restoran Mr.Chen",
                    Longitude = 16.00363f,
                    Latitude = 45.80217f,
                    CityId = cities[1].Id,
                    City = cities[1]
                },

                new Restaurant()
                {
                    Id = Guid.NewGuid(),
                    Name = "Osječki restoran",
                    Adress = "Osječka ulica",
                    Longitude = 28.374212f,
                    Latitude = 40.487858f,
                    CityId = cities[2].Id,
                    City = cities[2]
                },

                new Restaurant()
                {
                    Id = Guid.NewGuid(),
                    Name = "Riječki restoran",
                    Adress = "riječka ulica",
                    Longitude = 10.374212f,
                    Latitude = 50.487858f,
                    CityId = cities[3].Id,
                    City = cities[3]
                },
                new Restaurant()
                {
                    Id = Guid.NewGuid(),
                    Name = "Splitski restoran",
                    Adress = "Splitska ulica",
                    Longitude = 16.374212f,
                    Latitude = 60.487858f,
                    CityId = cities[4].Id,
                    City = cities[4]
                }
            };
        }
    }
}
