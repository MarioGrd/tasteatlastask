﻿using System;

namespace Rest.Shared.Models
{
    public class City
    {
        #region PropertyLengths

        public const int MaxNameLength = 50;
        public const int MaxCountryLength = 50;

        #endregion

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
    }
}