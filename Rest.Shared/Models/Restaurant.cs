﻿using System;

namespace Rest.Shared.Models
{
    public class Restaurant
    {
        #region PropertyLengths

        public const int MaxNameLength = 50;
        public const int MaxAdressLength = 50;

        #endregion

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Adress { get; set; }
        public float Longitude { get; set; }
        public float Latitude { get; set; }
        public Guid CityId { get; set; }
        public virtual City City { get; set; }
    }
}
