﻿namespace Rest.Shared.Common
{
    public class PagedQuery
    {
        const int _pageSize = 10;

        public PagedQuery(int pageIndex)
        {
            this.PageIndex = pageIndex;
            this.Skip = this.CalculateSkippedEntites();
        }

        public int Skip { get; set; }
        public int PageSize = _pageSize;
        public int PageIndex { get; set; }

        private int CalculateSkippedEntites()
        {
            return (this.PageIndex - 1) * this.PageSize;
        }
    }
}
