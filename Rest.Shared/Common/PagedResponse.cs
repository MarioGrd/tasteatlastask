﻿using System.Collections.Generic;

namespace Rest.Shared.Common
{
    public class PagedResponse<T>
        {
            public ICollection<T> Data { get; set; }
            public int Count { get; set; }
        }
}
