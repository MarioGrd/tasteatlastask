﻿using System;

namespace Rest.Shared.ViewModels
{
    public class AddRestaurant
    {
        public string Name { get; set; }
        public string Adress { get; set; }
        public float Longitude { get; set; }
        public float Latitude { get; set; }
        public Guid CityId { get; set; }
    }
}
