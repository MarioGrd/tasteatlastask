﻿using System;

namespace Rest.Shared.ViewModels
{
    public class RestaurantView
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Adress { get; set; }
        public float Longitude { get; set; }
        public float Latitude { get; set; }
        public double Distance { get; set; }
        public Guid CityId { get; set; }
        public string CityName { get; set; }
        public string Country { get; set; }

    }
}
