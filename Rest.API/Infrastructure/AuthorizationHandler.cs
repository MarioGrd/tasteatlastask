﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Rest.API.Infrastructure
{
    public class AuthorizationHandler : DelegatingHandler
    {
        string _username;

        /// <summary>
        /// Basic authentication.
        /// </summary>
        /// <param name="authenticationHeaderVal"></param>
        /// <returns></returns>
        private bool ValidateCredentials(AuthenticationHeaderValue authenticationHeaderVal)
        {
            try
            {
                if (authenticationHeaderVal != null
                    && !String.IsNullOrEmpty(authenticationHeaderVal.Parameter))
                {
                    string[] decodedCredentials
                    = Encoding.ASCII.GetString(Convert.FromBase64String(
                    authenticationHeaderVal.Parameter))
                    .Split(new[] { ':' });

                    // "MOCK"
                    if (decodedCredentials[0].Equals("username") && 
                        decodedCredentials[1].Equals("password"))
                    {
                        // Request authenticated.
                        _username = "User";
                        return true;
                    }
                }
                // Request not authenticated.
                return false;
            }
            catch
            {
                return false;
            }
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if (ValidateCredentials(request.Headers.Authorization))
            {
                Thread.CurrentPrincipal = new CustomPrincipal(_username);
                HttpContext.Current.User = new CustomPrincipal(_username);
            }

            //Allow the request to process further down the pipeline
            var response = await base.SendAsync(request, cancellationToken);

            if (response.StatusCode == HttpStatusCode.Unauthorized
                && !response.Headers.Contains("WwwAuthenticate"))
            {
                response.Headers.Add("WwwAuthenticate", "Basic");
            }

            return response;
        }
    }
}