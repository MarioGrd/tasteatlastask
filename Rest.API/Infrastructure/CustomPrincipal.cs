﻿using System.Security.Principal;

namespace Rest.API.Infrastructure
{
    public class CustomPrincipal : IPrincipal
    {
        private const string ApiClientRole = "ApiClientRole";

        public CustomPrincipal(string username)
        {
            this.Username = username;
            this.Identity = new GenericIdentity(username);
        }

        public string Username { get; set; }
        public IIdentity Identity { get; set; }

        public bool IsInRole(string role)
        {
            return role.Equals(ApiClientRole) ? true : false;
        }
    }
}