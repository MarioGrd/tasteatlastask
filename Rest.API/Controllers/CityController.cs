﻿using Rest.BLL.Services.Interface;
using System.Threading.Tasks;
using System.Web.Http;

namespace Rest.API.Controllers
{
    /// <summary>
    /// City controller.
    /// </summary>
    public class CityController : BaseController
    {
        private ICityService _cityService;

        public CityController(ICityService cityService)
        {
            this._cityService = cityService;
        }

        /// <summary>
        /// Gets cities.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IHttpActionResult> Get()
        {
            return await this.ProcessAsync(async () => await this._cityService.GetAllCitiesAsync());
        }
    }
}