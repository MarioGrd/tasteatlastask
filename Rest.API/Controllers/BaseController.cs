﻿using Rest.BLL.Services.Exceptions;
using System;
using System.Threading.Tasks;
using System.Web.Http;
using FluentValidation;

namespace Rest.API.Controllers
{
    
    public abstract class BaseController : ApiController
    {
        /// <summary>
        /// Base controller constructor.
        /// </summary>
        public BaseController()
        {
        }

        /// <summary>
        /// Method for processing requests.
        /// </summary>
        /// <param name="request">Request.</param>
        /// <returns></returns>
        protected async Task<IHttpActionResult> ProcessAsync(Func<Task> request)
        {
            return await this.HandleRequestAsync(async () =>
            {
                await request.Invoke();
                return this.Ok();
            });
        }

        /// <summary>
        /// Method for processing requests.
        /// </summary>
        /// <param name="request">Request.</param>
        /// <returns></returns>
        protected async Task<IHttpActionResult> ProcessAsync<T>(Func<Task<T>> request)
        {
            return await this.HandleRequestAsync(async () =>
            {
                var result = await request.Invoke();
                return this.Ok(result);
            });
        }

        /// <summary>
        /// Handles request, otherwise throws error.
        /// </summary>
        /// <param name="request">Request.</param>
        /// <returns></returns>
        private async Task<IHttpActionResult> HandleRequestAsync(Func<Task<IHttpActionResult>> request)
        {
            try
            {
                return await request();
            }
            catch (ValidationException exception)
            {
                foreach (var error in exception.Errors)
                    this.ModelState.AddModelError(error.PropertyName, error.ErrorMessage);

                return this.BadRequest(this.ModelState);
            }
            catch (LocationExistsException exception)
            {
                return this.BadRequest(exception.Message);
            }
            catch (ObjectNotFoundException exception)
            {
                return this.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                return this.BadRequest(exception.Message);
            }
        }
    }
}
