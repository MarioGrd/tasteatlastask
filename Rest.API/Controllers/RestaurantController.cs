﻿using Rest.BLL.Services.Interface;
using Rest.Shared.ViewModels;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace Rest.API.Controllers
{
    /// <summary>
    /// Restaurant controller.
    /// </summary>
    [Authorize]
    public class RestaurantController : BaseController
    {
        private IRestaurantService _restaurantService;

        public RestaurantController(IRestaurantService restaurantService)
        {
            this._restaurantService = restaurantService;
        }

        /// <summary>
        /// Filters restaurant by cityId.
        /// </summary>
        /// <param name="cityId">City identifier.</param>
        /// <param name="pageIndex">Current page index.</param>
        /// <param name="pageSize">Current page size.</param>
        /// <returns>Paged list of restaurants.</returns>
        [HttpGet]
        public async Task<IHttpActionResult> FilterRestourants([FromUri] Guid cityId, [FromUri]int pageIndex, [FromUri]int pageSize)
        {
            return await this.ProcessAsync(async () => await this._restaurantService.FilterRestourantsAsync(cityId, pageIndex, pageSize));
        }

        /// <summary>
        /// Gets nearest restaurants
        /// </summary>
        /// <param name="id">Restaurant identifier.</param>
        /// <returns>List of nearest restaurants</returns>
        [HttpGet]
        public async Task<IHttpActionResult> GetNearestRestaurants([FromUri] Guid id)
        {
            return
                await this.ProcessAsync(async () => await this._restaurantService.GetNearestRestaurants(id));
        }

        /// <summary>
        /// Creates new restaurant.
        /// </summary>
        /// <param name="restaurant">Restaurant model.</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IHttpActionResult> Post([FromBody] AddRestaurant restaurant)
        {
            return
                await this.ProcessAsync(async () => await this._restaurantService.AddRestourantAsync(restaurant));
        }
    }
}