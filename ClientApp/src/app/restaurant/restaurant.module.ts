import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../shared';
import { RestaurantComponent, RestaurantList, RestaurantDetails, CityComponent, AddRestaurantComponent } from './';
import { NgxPaginationModule } from 'ngx-pagination';

const restaurantRouting: ModuleWithProviders = RouterModule.forChild([
    {
        path: 'Restaurants',
        component: RestaurantComponent
            
    },
    {
        path: 'Restaurants/Add',
        component: AddRestaurantComponent
    }

]);

@NgModule({
    imports: [
        NgxPaginationModule,
        restaurantRouting,
        SharedModule
    ],
    providers: [],
    declarations: [
        RestaurantComponent,
        RestaurantList,
        RestaurantDetails,
        AddRestaurantComponent,
        CityComponent
        
    ]
})
export class RestaurantModule {}