import { Component, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Restaurant } from '../shared/models/restaurant.model';
import { Paged } from '../shared/models/paged.model';


@Component({
    selector: 'restaurant-list',
    templateUrl: './restaurant.list.component.html'
})
export class RestaurantList {
    @Input() pageNumber: number;
    @Input() restaurants: Observable<Paged<Restaurant>>;
    @Output() selected = new EventEmitter();
    @Output() pageChanged = new EventEmitter();
}