import { Component, OnChanges, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { AppStore, Restaurant, Paged, City, NearestRestaurant } from '../shared/models';
import { RestaurantService, CityService } from '../shared/services';

import 'rxjs/add/operator/map';

@Component({
    selector: 'restaurant',
    templateUrl: './restaurant.component.html',

})

export class RestaurantComponent {
    isTableVisiable: Boolean;
    isSelected: Boolean;
    pageNumber: number;
    restaurant: Observable<Restaurant>
    restaurants: Observable<Paged<Restaurant>>
    nearestRestaurants: Observable<NearestRestaurant>
    cities: Observable<City[]>
    city: City;

constructor(
        private restaurantService: RestaurantService,
        private cityService: CityService,
        private store: Store<AppStore>)
    {
        this.pageNumber = 1;

        this.cityService.getCities();
        this.cities = store.select('cities');
        this.restaurants = store.select('restaurants');
        this.restaurant = this.store.select('selectedRestaurant');
        this.nearestRestaurants = this.store.select('nearestRestaurant')
    }

    onCityChange(value: City) {
        this.isTableVisiable = true;
        this.city = value;
        this.restaurantService.loadRestaurants(this.city.id, this.pageNumber);
        this.isSelected = false;
    }

     getPage(value) {
        this.pageNumber = value;
        this.restaurantService.loadRestaurants(this.city.id, this.pageNumber);
        this.isSelected = false;
    }

    selectRestaurant(value)
    {
        this.store.dispatch({ type: 'SELECT_RESTAURANT', payload: value });
        this.isSelected = true;
        this.restaurantService.getNearestRestaurants(value.id);
    }

    selectNearest(value: NearestRestaurant) {
        let city = new City()
            city.id = value.cityId;
            city.name = value.cityName;
            city.country = value.country;

        let restaurant = new Restaurant();
            restaurant.id = value.id;
            restaurant.name = value.name;
            restaurant.adress = value.adress;
            restaurant.latitude = value.latitude;
            restaurant.longitude = value.longitude;
            restaurant.city = city;

        this.selectRestaurant(restaurant);
    }
}