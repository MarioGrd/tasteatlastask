import { Component, Input, Output, EventEmitter } from'@angular/core';
import { AppStore, Restaurant, Paged, City, NearestRestaurant } from '../shared/models';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { RestaurantService } from '../shared/services';



@Component({
    selector: 'restaurant-details',
    templateUrl: './restaurant.details.component.html'
})
export class RestaurantDetails {
    @Input() selectedRestaurant = new EventEmitter;
    @Input() nearestRestaurants = new EventEmitter;
    @Output() selected = new EventEmitter();
}