import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { AppStore } from '../shared';
import { FormControl }   from '@angular/forms';

import { City } from '../shared/models';

import 'rxjs/add/operator/map';

@Component({
    selector: 'city-component',
    templateUrl: './city.component.html'
})

export class CityComponent {
    @Output() changeCity = new EventEmitter();
    @Input() cities: City[];
}
