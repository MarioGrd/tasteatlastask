import { Component, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { FormBuilder, FormControl, FormGroup  } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppStore, City } from '../shared/models';
import { CityService, RestaurantService } from '../shared/services';
import { Router } from '@angular/router';

@Component({
    selector: 'restaurant-add',
    templateUrl: './restaurant.add.component.html'
})
export class AddRestaurantComponent {
    private isSubmitting: boolean = false;
    private cities: Observable<City[]>;
    private restaurantForm: FormGroup;

    public constructor(
        private router: Router,
        private store: Store<AppStore>,
        private restaurantService: RestaurantService,
        private cityService: CityService,
        private formBuilder: FormBuilder) {}
    
    ngOnInit() {

        this.cityService.getCities();
        this. cities = this.store.select('cities');

        this.restaurantForm = this.formBuilder.group({
            'name' : [''],
            'adress': [''],
            'longitude': [''],
            'latitude': [''],
            'cityId': ['']
        });
    }

    public submitForm() {
        this.isSubmitting = true;

        this.restaurantService
            .addRestaurant(this.restaurantForm.value)
                .subscribe(
                    success => {
                        alert('Successfull');
                        this.router.navigateByUrl('/Restaurants')}, 
                    error => this.httpCallError(error, this.restaurantForm));
        
        
        this.isSubmitting = false;
    }

    public httpCallError(error: any, form: FormGroup = null) {

      var errorMessage: string;
      
      for (var key in error.modelState) {
        errorMessage += '\n' + error.modelState[key];

        var control = form.get(key);
        
        if (control) {
            control.setErrors({
                'remote': error.modelState[key][0]
                });
        }
      }
    }
}