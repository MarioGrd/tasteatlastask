import { BrowserModule } from '@angular/platform-browser';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';


import {
  HeaderComponent,
  FooterComponent,

  RestaurantService,
  CityService,

  SharedModule,
} from './shared';

import { RestaurantModule } from './restaurant/restaurant.module';
import { restaurants } from './shared/store/restaurants.store';
import { selectedRestaurant } from './shared/store/restaurant.selected.store';
import { nearestRestaurant } from './shared/store/restaurant.nearest.store';
import { cities } from './shared/store/city.store';

const rootRouting: ModuleWithProviders = RouterModule.forRoot([
  
	{ path: '**', redirectTo: '' }
], { useHash: true });

@NgModule({
declarations: 
[
    AppComponent,
    HeaderComponent,
    FooterComponent
],
  
imports: 
[
    BrowserModule,
    StoreModule.provideStore({
      cities,
      restaurants,
      selectedRestaurant,
      nearestRestaurant}),
    rootRouting,
    FormsModule,
    HttpModule,
    SharedModule,
    RestaurantModule
],
  
providers: 
[
    RestaurantService,
    CityService
],
  
bootstrap: [AppComponent]

})

export class AppModule { }
