import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import {HttpModule, Http, XHRBackend, RequestOptions} from '@angular/http';
import { HttpFactory } from './services/http.factory';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule
  ],
  declarations: [
  ],
  providers: [
    {
            provide: Http,
            useFactory: HttpFactory,
            deps: [XHRBackend, RequestOptions]
    }
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule,
  ]
})
export class SharedModule { }