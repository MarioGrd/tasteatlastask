export const cities = (state: any = [], {type, payload}) => {
    switch(type) {
        case 'GET_CITIES':
            return payload;
        default:
            return state;
    }
}