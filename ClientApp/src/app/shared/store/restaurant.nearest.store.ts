export const nearestRestaurant = (state: any = [], {type, payload}) => {
    switch(type) {
        case 'GET_NEAREST_RESTAURANTS':
            return payload;
        default:
            return state;
    }
}