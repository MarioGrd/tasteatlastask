export const selectedRestaurant = (state: any = null, {type, payload}) => {
    switch (type) {
        case 'SELECT_RESTAURANT':
            return payload;
        default:
            return state;
    }
}