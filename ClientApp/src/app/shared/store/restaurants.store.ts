
export const restaurants = (state: any = [], {type, payload}) => {
    switch(type) {
        case 'FILTER_RESTAURANTS':
            return payload;
        case 'ADD_RESTAURANT':
            return [...state, payload];
        default:
            return state;
    }
}