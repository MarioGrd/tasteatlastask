import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { 
    Headers, 
    Http, 
    Response, 
    URLSearchParams, 
    ConnectionBackend,
    Request,
    RequestOptions,
    RequestOptionsArgs } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class HttpService extends Http {

    constructor(backend: ConnectionBackend, defaultOptions: RequestOptions) {
        super(backend, defaultOptions);
    }

    request(url: string | Request, options?: RequestOptionsArgs) : Observable<Response> {
        return super.request(url, options);
    }

    get(url: string, params: any, options?: RequestOptionsArgs) : Observable<Response> {
        url = this.updateUrl(url);
        return super.get(url, this.getRequestOptionArgs(options, params))
            .map(this.extractData)
            .catch(this.formatErrors);
    }

    post(url: string, body: Object = {}, options?:RequestOptionsArgs) : Observable<Response>{
        url = this.updateUrl(url);
        return super.post(url, JSON.stringify(body), this.getRequestOptionArgs(options))
            .map(this.extractData)
            .catch(this.formatErrors);
    }

    put(url: string, body: Object = {}, options?: RequestOptionsArgs) : Observable<Response>{
        url = this.updateUrl(url);
        return super.put(url, JSON.stringify(body), this.getRequestOptionArgs(options))
            .map(this.extractData)
            .catch(this.formatErrors)
    }

    delete(url: string, options?: RequestOptionsArgs) : Observable<Response> {
        url = this.updateUrl(url);
        return super.delete(url, this.getRequestOptionArgs(options))
            .map(this.extractData)
            .catch(this.formatErrors)
    }

    private updateUrl(req: string) {
        return  environment.api_url + req;
    }

    private getRequestOptionArgs(options?: RequestOptionsArgs, searchParams?: any) : RequestOptionsArgs {
        
        if (options == null) {
            options = new RequestOptions();
        }
        if (options.headers == null) {
            options.headers = this.setHeaders();
        }

        if (searchParams != null) {
            options.search = this.setSearchParams(searchParams);
        }
        return options;
    }


setSearchParams(param: any): URLSearchParams {
      let params: URLSearchParams = new URLSearchParams();
      for (var key in param) {
          if (param.hasOwnProperty(key)) {
              let val = param[key];
              params.set(key, val);
          }
      }

      return params;
    }

private setHeaders(): Headers {
    let headersConfig = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': '*',
      'Access-Control-Allow-Headers':'*',
      'API_KEY': 'SuperSecretKey',
      'Authorization':'Basic dXNlcm5hbWU6cGFzc3dvcmQ='
    };

    return new Headers(headersConfig);
  }

  private formatErrors(error: any) {
      
        let errors = { 
            'message': '',
            'modelState': {}};
        
        errors = JSON.parse(error._body);

        if (!errors.modelState) {
            alert(errors.message);
        }

     return Observable.throw(error.json());
  }

  private extractData(res: Response) {
    let body;
    if (res.text()) {
      body = res.json();
    }
    return body || {};
  }
}