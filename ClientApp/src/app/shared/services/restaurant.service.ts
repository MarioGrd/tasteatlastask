import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { AppStore } from '../models';
import 'rxjs/add/operator/map';
import { FormGroup  } from '@angular/forms';

import { Restaurant } from '../models';

@Injectable()
export class RestaurantService {
    
    private baseUrl: string;

    constructor(private http: Http, private store: Store<AppStore>) {
        this.baseUrl = 'Restaurant';
    }

    addRestaurant(data) : Observable<any> {
        return this.http.post(this.baseUrl, data)
    }

    loadRestaurants(cityId: string, pageIndex?: number) {

        let pageSize = 3;
        
        if (!pageIndex)
            pageIndex = 1;
        
        let searchParams = {'cityId': cityId, 'pageIndex': pageIndex, 'pageSize': pageSize}

        this.http.get(this.baseUrl, searchParams)
            .map(
                payload => ({type: 'FILTER_RESTAURANTS', payload}))
            .subscribe(
                action => this.store.dispatch(action));
    }

    getNearestRestaurants(restaurantId: string) {
        
        let params = {'id': restaurantId}
        this.http.get(this.baseUrl, params)

            .subscribe(
                action => {
                    this.store.dispatch({type: 'GET_NEAREST_RESTAURANTS', payload: action });
                });
    }
}