import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { Store } from '@ngrx/store';
import { AppStore } from '../models';



@Injectable()
export class CityService {
    
    private baseUrl;

    constructor(
        private http: Http, private store: Store<AppStore>) {
            this.baseUrl = 'City';
    }

    getCities() {
        this.http.get(this.baseUrl)
        .map(
                payload => ({type: 'GET_CITIES', payload}))
            .subscribe(
                action => this.store.dispatch(action));
    }
}