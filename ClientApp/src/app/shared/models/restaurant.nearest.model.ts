export class NearestRestaurant {
    id: string;
    name: string;
    adress: string;
    longitude: number;
    latitude: number;
    distance: number;
    cityId: string;
    cityName: string;
    country: string;
}