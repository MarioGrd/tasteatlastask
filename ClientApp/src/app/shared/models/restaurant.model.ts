import { City } from './city.model';

export class Restaurant {
    id: string;
    name: string;
    adress: string;
    longitude: number;
    latitude: number;
    city: City;
}