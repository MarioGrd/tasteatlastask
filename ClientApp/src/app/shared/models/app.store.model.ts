import { Restaurant } from './restaurant.model';
import { NearestRestaurant } from './restaurant.nearest.model';
import { Paged } from './paged.model';
import { City } from './city.model';

export interface AppStore {
    cities: City[]
    restaurants: Paged<Restaurant>;
    selectedRestaurant: Restaurant;
    nearestRestaurants: NearestRestaurant[];
}