﻿using Autofac;
using Autofac.Integration.WebApi;
using Rest.BLL.Services;
using Rest.BLL.Services.Interface;
using Rest.DAL.Infrastructure.Common;
using Rest.DAL.Infrastructure.GenericRepository;
using Rest.DAL.Infrastructure.GenericRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Rest.BLL.Infrastructure
{
    public class AutofacConfiguration
    {
        public static IContainer Container;
        public static void Initialize(HttpConfiguration config)
        {
            Initialize(config, RegisterServices(new ContainerBuilder()));
        }

        public static void Initialize(HttpConfiguration config, IContainer container)
        {
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        private static IContainer RegisterServices(ContainerBuilder builder)
        {
            builder.RegisterApiControllers(Assembly.Load("Rest.API"));

            // EF 
            builder.RegisterType<RestaurantContext>()
                   .As<DbContext>()
                   .InstancePerRequest();

            builder.RegisterType<DbFactory>()
                .As<IDbFactory>()
                .InstancePerRequest();

            builder.RegisterType<UnitOfWork>()
                .As<IUnitOfWork>()
                .InstancePerRequest();

            builder.RegisterGeneric(typeof(Repository<>))
                   .As(typeof(IRepository<>))
                   .InstancePerRequest();

            // Services
            builder.RegisterType<RestaurantService>()
                .As<IRestaurantService>()
                .InstancePerRequest();

            builder.RegisterType<CityService>()
                .As<ICityService>()
                .InstancePerRequest();

            Container = builder.Build();

            return Container;
        }
    }
}
