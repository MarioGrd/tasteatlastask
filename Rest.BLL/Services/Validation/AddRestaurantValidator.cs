﻿using FluentValidation;
using Rest.Shared.Models;
using Rest.Shared.ViewModels;

namespace Rest.BLL.Services.Validation
{
    /// <summary>
    /// Validates AddRestaurant model.
    /// </summary>
    public class AddRestaurantValidator : AbstractValidator<AddRestaurant>
    {
        public AddRestaurantValidator()
        {

            RuleFor(r => r.Name)
                .NotEmpty()
                .WithMessage("Name is empty.")
                .MaximumLength(Restaurant.MaxNameLength)
                .WithMessage($"Name is larger than {Restaurant.MaxNameLength} ");

            RuleFor(r => r.Adress)
                .NotEmpty()
                .WithMessage("Adress is empty.")
                .MaximumLength(Restaurant.MaxAdressLength)
                .WithMessage($"Name is larger than {Restaurant.MaxAdressLength} "); ;

            RuleFor(r => r.CityId)
                .NotEmpty()
                .WithMessage("City is empty.");

            RuleFor(r => r.Latitude)
                .NotEmpty()
                .WithMessage("Latitude is empty.")
                .GreaterThan(0)
                .WithMessage("Latitude must be greater then 0.");

            RuleFor(r => r.Longitude)
                .NotEmpty()
                .WithMessage("Longitude is empty.")
                .GreaterThan(0)
                .WithMessage("Longitude must be greater then 0.");
        }
    }
}
