﻿using Rest.Shared.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rest.BLL.Services.Interface
{
    public interface ICityService
    {
        Task<List<City>> GetAllCitiesAsync();
    }
}
