﻿using Rest.Shared.Common;
using Rest.Shared.Models;
using Rest.Shared.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rest.BLL.Services.Interface
{
    public interface IRestaurantService
    {
        Task<PagedResponse<Restaurant>> FilterRestourantsAsync(Guid cityId, int pageIndex, int pageSize);
        Task<List<RestaurantView>> GetNearestRestaurants(Guid id);
        Task AddRestourantAsync(AddRestaurant restaurant);
    }
}
