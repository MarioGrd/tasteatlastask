﻿using Rest.BLL.Services.Interface;
using Rest.DAL.Infrastructure.Common;
using Rest.Shared.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rest.BLL.Services
{
    public class CityService : ICityService
    {
        private IUnitOfWork _unitOfWork;

        public CityService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public async Task<List<City>> GetAllCitiesAsync()
        {
            return
                await this._unitOfWork.CityRepository.GetAllAsync();
        }
    }
}
