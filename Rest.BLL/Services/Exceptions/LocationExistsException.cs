﻿using System;

namespace Rest.BLL.Services.Exceptions
{
    public class LocationExistsException : Exception
    {
        public LocationExistsException(string message) : base(message)
        {
        }
    }
}
