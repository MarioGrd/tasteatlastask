﻿using System;

namespace Rest.BLL.Services.Exceptions
{
    public class ObjectNotFoundException : Exception
    {
        public ObjectNotFoundException(string msg) : base(msg) { }
    }
}
