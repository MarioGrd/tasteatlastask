﻿using FluentValidation;
using Rest.BLL.Services.Exceptions;
using Rest.BLL.Services.Interface;
using Rest.BLL.Services.Validation;
using Rest.DAL.Infrastructure.Common;
using Rest.Shared.Common;
using Rest.Shared.Models;
using Rest.Shared.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rest.BLL.Services
{
    public class RestaurantService : IRestaurantService
    {
        private IUnitOfWork _unitOfWork;

        public RestaurantService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Validates and adds restaurant.
        /// </summary>
        /// <param name="restaurant">Restaurant.</param>
        /// <returns>Void.</returns>
        public async Task AddRestourantAsync(AddRestaurant restaurant)
        {
            var validator = new AddRestaurantValidator();
            validator.ValidateAndThrow(restaurant);

            if (await this._unitOfWork.RestaurantRepository.LocationExists(restaurant.Longitude, restaurant.Latitude))
                throw new LocationExistsException("Location already exists.");

            var r = new Restaurant()
            {
                Id = Guid.NewGuid(),
                Name = restaurant.Name,
                Adress = restaurant.Adress,
                Latitude = restaurant.Latitude,
                Longitude = restaurant.Longitude,
                CityId = restaurant.CityId
            };

            this._unitOfWork.RestaurantRepository.Add(r);

            await this._unitOfWork.CommitAsync();
        }

        /// <summary>
        /// Filters restaurant by city.
        /// </summary>
        /// <param name="cityId">City id.</param>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="pageSize">Page size.</param>
        /// <returns></returns>
        public async Task<PagedResponse<Restaurant>> FilterRestourantsAsync(Guid cityId, int pageIndex, int pageSize)
        {
            if (!await this._unitOfWork.CityRepository.AnyAsync(u => u.Id == cityId))
                throw new ObjectNotFoundException($"{nameof(City)} not found!");

            return 
                await this._unitOfWork.RestaurantRepository
                    .FilterByCityAsync(cityId, pageIndex, pageSize);
        }

        /// <summary>
        /// Gets nearest restaurants.
        /// </summary>
        /// <param name="id">Restaurant id.</param>
        /// <returns>List of nearest restaurants.</returns>
        public async Task<List<RestaurantView>> GetNearestRestaurants(Guid id)
        {
            if (!await this._unitOfWork.RestaurantRepository.AnyAsync(u => u.Id == id))
                throw new ObjectNotFoundException($"{nameof(Restaurant)} not found!");

            return
                await this._unitOfWork.RestaurantRepository.GetNearestRestourants(id);
        }
    }
}
